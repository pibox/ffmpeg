# ---------------------------------------------------------------
# Build piboxwww
# ---------------------------------------------------------------

# Retrieve package
.$(X264_T)-get $(X264_T)-get: 
	@if [ ! -f $(ARCDIR)/$(X264_ARCHIVE) ]; then \
		$(MSG) "================================================================"; \
		$(MSG2) "Retrieving files" $(EMSG); \
		$(MSG) "================================================================"; \
		mkdir -p $(ARCDIR); \
		cd $(ARCDIR) && wget $(X264_URL); \
	else \
		$(MSG3) "X264 source is cached" $(EMSG); \
	fi
	@touch .$(subst .,,$@)

.$(X264_T)-get-patch $(X264_T)-get-patch: .$(X264_T)-get
	@touch .$(subst .,,$@)

# Unpack packages
.$(X264_T)-unpack $(X264_T)-unpack: .$(X264_T)-get-patch
	@mkdir -p $(X264_SRCDIR)
	@cd $(ARCDIR) && tar -C $(X264_SRCDIR) --strip-components=1 -xf $(X264_ARCHIVE)
	@touch .$(subst .,,$@)

# Apply patches
.$(X264_T)-patch $(X264_T)-patch: .$(X264_T)-unpack
	@touch .$(subst .,,$@)

.$(X264_T)-init $(X264_T)-init: 
	@make build-verify
	@make .$(X264_T)-patch
	@touch .$(subst .,,$@)

.$(X264_T)-config $(X264_T)-config: 
	@cd $(X264_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
		CFLAGS="-O2 -pipe -mcpu=arm1176jzf-s -mfpu=vfp -mfloat-abi=hard -w -I$(SD)/usr/include" \
		LDFLAGS="-L$(SD)/usr/lib -L$(SD)/lib --sysroot=$(SD)/" \
		./configure --host=arm-unknown-linux-gnueabi \
			--cross-prefix="$(XCC_PREFIXDIR)/bin/arm-unknown-linux-gnueabi-" \
			--enable-static

# Build the package
$(X264_T): .$(X264_T)

.$(X264_T): .$(X264_T)-init 
	@make --no-print-directory $(X264_T)-config
	@$(MSG) "================================================================"
	@$(MSG2) "Building X264" $(EMSG)
	@$(MSG) "================================================================"
	@cd $(X264_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
		make
	@touch .$(subst .,,$@)

$(X264_T)-pkg: .$(X264_T)
	@mkdir -p $(PKGDIR)/install
	@cd $(X264_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
		DESTDIR=$(PKGDIR)/install \
		make install

# Clean out a cross compiler build but not the CT-NG package build.
$(X264_T)-clean: 
	@if [ "$(X264_SRCDIR)" != "" ]; then \
		cd $(X264_SRCDIR) && make distclean; \
	fi
	@rm -f .$(X264_T) .$(X264_T)-get

# Clean out everything associated with X264
$(X264_T)-clobber: 
	@rm -rf $(X264_BLDDIR) $(X264_SRCDIR)
	@rm -f .$(X264_T)-config .$(X264_T)-init .$(X264_T)-patch \
		.$(X264_T)-unpack .$(X264_T)-get .$(X264_T)-get-patch
	@rm -f .$(X264_T) 

