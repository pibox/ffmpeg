# ---------------------------------------------------------------
# Build ffmpeg
# ---------------------------------------------------------------

root-verify:
	@if [ $(UID) -ne 0 ]; then \
		$(MSG11) "You must be root." $(EMSG); \
		exit 1; \
	fi

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

build-verify:
	@if [ "$(CROSS_COMPILER)" = "" ] || [ ! -f $(CROSS_COMPILER) ]; then \
		$(MSG11) "Can't find cross toolchain.  Try setting XI= on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ "$(SD)" = "" ] || [ ! -d $(SD) ]; then \
		$(MSG11) "Can't find staging tree.  Try setting SD= on the command line." $(EMSG); \
		exit 1; \
	fi

# Retrieve package
.$(FFMPEG_T)-get $(FFMPEG_T)-get: 
	@if [ ! -f $(ARCDIR)/$(FFMPEG_ARCHIVE) ]; then \
		$(MSG) "================================================================"; \
		$(MSG2) "Retrieving files" $(EMSG); \
		$(MSG) "================================================================"; \
		mkdir -p $(ARCDIR); \
		cd $(ARCDIR) && wget $(FFMPEG_URL); \
	else \
		$(MSG3) "FFMPEG source is cached" $(EMSG); \
	fi
	@touch .$(subst .,,$@)

.$(FFMPEG_T)-get-patch $(FFMPEG_T)-get-patch: .$(FFMPEG_T)-get
	@touch .$(subst .,,$@)

# Unpack packages
.$(FFMPEG_T)-unpack $(FFMPEG_T)-unpack: .$(FFMPEG_T)-get-patch
	@mkdir -p $(BLDDIR)
	@cd $(ARCDIR) && tar -C $(BLDDIR) -xf $(FFMPEG_ARCHIVE)
	@touch .$(subst .,,$@)

# Apply patches
.$(FFMPEG_T)-patch $(FFMPEG_T)-patch: .$(FFMPEG_T)-unpack
	@touch .$(subst .,,$@)

.$(FFMPEG_T)-init $(FFMPEG_T)-init: 
	@make build-verify
	@make .$(FFMPEG_T)-patch
	@touch .$(subst .,,$@)

.$(FFMPEG_T)-config $(FFMPEG_T)-config: 
	@cd $(FFMPEG_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
		CFLAGS="-O2 -pipe -mcpu=arm1176jzf-s -mfpu=vfp -mfloat-abi=hard -w -I$(SD)/usr/include -I$(X264_SRCDIR)" \
		LDFLAGS="-L$(SD)/usr/lib -L$(SD)/lib -L$(FFMPEG_SRCDIR) --sysroot=$(SD)/" \
		./configure --enable-cross-compile \
			--cross-prefix="$(XCC_PREFIXDIR)/bin/arm-unknown-linux-gnueabi-" \
			--arch=armel --target-os=linux --enable-gpl --enable-libx264 \
			--extra-cflags="-I$(X264_SRCDIR)" --extra-ldflags="-L$(X264_SRCDIR)" \
			--disable-opencl --extra-libs=-ldl

# Build the package
$(FFMPEG_T): .$(FFMPEG_T)

.$(FFMPEG_T): .$(X264_T) .$(FFMPEG_T)-init 
	@make --no-print-directory $(FFMPEG_T)-config
	@$(MSG) "================================================================"
	@$(MSG2) "Building FFMPEG" $(EMSG)
	@$(MSG) "================================================================"
	@cd $(FFMPEG_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
		make
	@touch .$(subst .,,$@)

$(FFMPEG_T)-files:
	@$(MSG) "================================================================"
	@$(MSG2) "FFMPEG Files" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(FFMPEG_SRCDIR)

# Package it as an opkg 
pkg $(FFMPEG_T)-pkg: .$(FFMPEG_T) 
	@make --no-print-directory root-verify opkg-verify
	@make --no-print-directory $(X264_T)-pkg
	@mkdir -p $(PKGDIR)/install
	@cd $(FFMPEG_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
		DESTDIR=$(PKGDIR)/install \
		make install
	@mkdir -p $(PKGDIR)/opkg/ffmpeg/CONTROL
	@cp -ar $(PKGDIR)/install/* $(PKGDIR)/opkg/ffmpeg/
	@cp $(SRCDIR)/opkg/control $(PKGDIR)/opkg/ffmpeg/CONTROL/control
	@cp $(SRCDIR)/opkg/debian-binary $(PKGDIR)/opkg/ffmpeg/CONTROL/debian-binary
	@chown -R root.root $(PKGDIR)/opkg/ffmpeg/
	@cd $(PKGDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O ffmpeg
	@cp $(PKGDIR)/opkg/*.opk $(PKGDIR)/
	@rm -rf $(PKGDIR)/opkg

# Clean the packaging
pkg-clean $(FFMPEG_T)-pkg-clean:
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm -rf $(PKGDIR); fi

# Clean out a cross compiler build but not the CT-NG package build.
$(FFMPEG_T)-clean: $(FFMPEG_T)-pkg-clean
	@if [ "$(FFMPEG_SRCDIR)" != "" ] && [ -d "$(FFMPEG_SRCDIR)" ]; then \
		cd $(FFMPEG_SRCDIR) && make distclean; \
	fi
	@rm -f .$(FFMPEG_T) 

# Clean out everything associated with FFMPEG
$(FFMPEG_T)-clobber: root-verify
	@rm -rf $(PKGDIR) 
	@rm -rf $(FFMPEG_SRCDIR) 
	@rm -f .$(FFMPEG_T)-config .$(FFMPEG_T)-init .$(FFMPEG_T)-patch \
		.$(FFMPEG_T)-unpack .$(FFMPEG_T)-get .$(FFMPEG_T)-get-patch
	@rm -f .$(FFMPEG_T) 

